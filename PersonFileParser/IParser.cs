﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonFileParser
{
    interface IParser<T>
    {
        List<T> Parse(String content);
    }
}
